# Spreadsheet CMS

clone project to your own server

change directory in to the folder you just cloned

create a virtual environment for python3 in that folder

	virtualenv -p python3 venv

start virtualenv
	
	source venv/bin/activate

Install requirements

	pip install -r requirements.text

You will need to navigate to app/themes/ folder and clone the default theme 

Navigate to the root folder of the project and run server

	python run.py

navigate to the themes folder and create your theme. This will not be tracked in the current repo so you will need to create a new repo for this. The default theme that is there should give you a guide as to how to implement this. 

