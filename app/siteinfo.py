info = {
	"facebook": "spreadsheetcms",
	"twitter": "spreadsheetcms",
	"instagram": "spreadsheetcms",
	"linkedin": "spreadsheetcms",
	"youtube" : "spreadsheetcms",
	"title": "McBrides Gun Shop",
	"description": "",
	"logo": "https://cdn.shopify.com/s/files/1/1906/7097/files/mcbrideslogo.png?4628088095932690934",
	"favicon" : "https://cdn.shopify.com/s/files/1/1906/7097/files/favicon-mcbrides.png?4628088095932690934",
	"phone" : "353906478205",
	"email" : "info@mcbrides.ie",
	"mobile" : "353 87 269 6647",
	"address" : "Empire State Building, 5th Ave, New York",
	"footertext" : "McBrides Gunshop supply all leading brands in field sport. Browning, CZ, Ruger, AYA, Beretta, Remington. We also have a wide range of clothing on sale.",	
	"mapboxapi" : "pk.eyJ1IjoiZG9uc2hpbmUiLCJhIjoiY2oyZzN1aWRsMDAwNzQ0bjJvdHc0dGZmeiJ9.skGwcKzs1sU_wU09slXvwg",
	"mapmarkercolor": "#ee4431"
}