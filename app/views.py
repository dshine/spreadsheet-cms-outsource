from flask import Flask, session, redirect, url_for, escape, request, abort
from app import app
from flask_theme import setup_themes, render_theme_template, get_theme, get_themes_list, Theme 
import urllib.parse
from markupsafe import Markup # this is for the urlencode filter function
from jinja2 import Environment, FileSystemLoader

import requests, json, os, html2text

from . import sitenav, siteinfo, sitedata

menu = sitenav.menu
info = siteinfo.info
data = sitedata.data
content = sitedata.data


file_loader = FileSystemLoader('templates')

@app.template_filter('urlencode')
def urlencode_filter(s):
	if type(s) == 'Markup':
		s = s.unescape()
	s = s.encode('utf8')
	s = urllib.parse.quote_plus(s)
	return Markup(s)

@app.route('/')
def index():
	data = sitedata.data
	css = "demo.css"
	content = default_site
	data = data['home']

	'''
	sheetmap, menu, info = getGoogleData(content['spreadsheet_id'])
	for items in sheetmap:
		if items['id'].lower() == 'index' or items['id'].lower() == 'home':
			data = sheetToJson(content['spreadsheet_id'], items['value'])
	'''
	return render_template('index.html', content=content, nav=menu, info=info, data=data, config=content, css=css)

@app.route('/pages/<slug>')
def pages(slug):
	data = sitedata.data
	css = "demo.css"
	content = default_site
	if slug in data:
		data = data[slug]
	else:
		abort(404)
	return render_template('page.html', content=content, nav=menu, info=info, data=data, sheetmap=data, css=css)


@app.errorhandler(404)
def notFound(error):
	content = {'title': 'Page Not Found'}
	menu = info = ""
	#return render_template('404.html', content=content, nav=menu, info=info, error=error), 404
	return render_template('404.html', content=content, nav=menu, info=info, error=error), 404

@app.errorhandler(500)
def internalerror(error):
	content = {'title': 'Hueston we have a problem'}
	menu = info = ""
	return render_template('500.html', content=content, nav=error.description, info=info, error=error), 500

def render_template(template, **context):
	theme = "default"
	if 'theme' in context['info']:
		theme = context['info']['theme']
	t = Theme(os.path.join(app.root_path, 'themes', theme))
	return render_theme_template(t.identifier, template, **context)

default_site = {
	"title" : "Some Title",
	"spreadsheet_id" : "1LHvbomhy3VZz05DMP-JQDpssP5EjIA31MIuL8Rh461A",
	"theme" : "default"
}
