data = {
	"home" : [
		{
			"type" : "seoBasic",
			"title" : "Basic - STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "seoFacebook",
			"title" : "STEM Berlin Unofficial - facebook",
			"text" : "facebook - This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "seoTwitter",
			"title" : "Twitter - STEM Berlin Unofficial",
			"text" : "twitter - This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "jumbotron",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "heroImage",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "imageText",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "textImage",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "carousel",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "",
			"linktext" : "",
			"blocks" : [
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "",
					"linktext" : ""
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "",
					"linktext" : ""
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "",
					"linktext" : ""
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "",
					"linktext" : ""
				}
			]
		}
	],
	"contact-us" : [
		{
			"type" : "jumbotron",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "heroImage",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "imageText",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "textImage",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "carousel",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "",
			"linktext" : "",
			"blocks" : [
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "",
					"linktext" : ""
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "",
					"linktext" : ""
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "",
					"linktext" : ""
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "",
					"linktext" : ""
				}
			]
		}
	],
	"card" : [
		{
			"type" : "textBlock",
			"title" : "Card",
			"text" : "Below is the layout",
			"imgurl" : "",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "card",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "https://www.google.com",
			"linktext" : "Call to Action",
			"blocks" : [
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				}
			]
		}
	],
	"carousel" : [
		{
			"type" : "textBlock",
			"title" : "carousel",
			"text" : "Below is the layout",
			"imgurl" : "",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "carousel",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "https://www.google.com",
			"linktext" : "Call to Action",
			"blocks" : [
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				}
			]
		}
	],
	"gallery" : [
		{
			"type" : "textBlock",
			"title" : "Gallery",
			"text" : "Below is the layout",
			"imgurl" : "",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "gallery",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "https://www.google.com",
			"linktext" : "Call to Action",
			"blocks" : [
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				}
			]
		}
	],
	"heroImage" : [
		{
			"type" : "textBlock",
			"title" : "heroImage",
			"text" : "Below is the layout",
			"imgurl" : "",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "heroImage",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "https://www.google.com",
			"linktext" : "Call to Action",
			"blocks" : [
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				}
			]
		}
	],
	"imageText" : [
		{
			"type" : "textBlock",
			"title" : "imageText",
			"text" : "Below is the layout",
			"imgurl" : "",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "imageText",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "https://www.google.com",
			"linktext" : "Call to Action",
			"blocks" : [
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				}
			]
		}
	],
	"infoBlocks" : [
		{
			"type" : "textBlock",
			"title" : "infoBlocks",
			"text" : "Below is the layout",
			"imgurl" : "",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "infoBlocks",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "https://www.google.com",
			"linktext" : "Call to Action",
			"blocks" : [
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				}
			]
		}
	],
	"infoBlocksImageCenter" : [
		{
			"type" : "textBlock",
			"title" : "infoBlocksImageCenter",
			"text" : "Below is the layout",
			"imgurl" : "",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "infoBlocksImageCenter",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "https://www.google.com",
			"linktext" : "Call to Action",
			"blocks" : [
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				}
			]
		}
	],
	"jumbotron" : [
		{
			"type" : "textBlock",
			"title" : "jumbotron",
			"text" : "Below is the layout",
			"imgurl" : "",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "jumbotron",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "https://www.google.com",
			"linktext" : "Call to Action",
			"blocks" : [
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				}
			]
		}
	],
	"map" : [
		{
			"type" : "textBlock",
			"title" : "map",
			"text" : "Below is the layout",
			"imgurl" : "",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "map",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "https://www.google.com",
			"linktext" : "Call to Action",
			"blocks" : [
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				}
			]
		}
	],
	"mapContact" : [
		{
			"type" : "textBlock",
			"title" : "mapContact",
			"text" : "Below is the layout",
			"imgurl" : "",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "mapContact",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "https://www.google.com",
			"linktext" : "Call to Action",
			"blocks" : [
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				}
			]
		}
	],
	"textBlock" : [
		{
			"type" : "textBlock",
			"title" : "textBlock",
			"text" : "Below is the layout",
			"imgurl" : "",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "textBlock",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "https://www.google.com",
			"linktext" : "Call to Action",
			"blocks" : [
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				}
			]
		}
	],
	"textImage" : [
		{
			"type" : "textBlock",
			"title" : "textImage",
			"text" : "Below is the layout",
			"imgurl" : "",
			"linkurl" : "",
			"linktext" : ""
		},
		{
			"type" : "textImage",
			"title" : "STEM Berlin Unofficial",
			"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
			"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
			"linkurl" : "https://www.google.com",
			"linktext" : "Call to Action",
			"blocks" : [
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				},
				{
					"title" : "STEM Berlin Unofficial",
					"text" : "This is an unoffical website from the STEM Dept at CIEE Berlin. It as a showcase of projects students have worked on.",
					"imgurl" : "https://www.ciee.org/sites/default/files/styles/607h/public/content/user-uploads/MSmith/6a010536fa9ded970b01bb086c509c970d-800wi_copy.jpg?itok=kDu8wBSm",
					"linkurl" : "https://www.google.com",
					"linktext" : "Call to Action"
				}
			]
		}
	]
}