menu = [
	{
		"url" : "/",
		"label" : "home"
	},
	{
		"url" : "/",
		"label" : "sections",
		"menu" : [
			{
				"url" : "/pages/card",
				"label" : "card"
			},
			{
				"url" : "/pages/carousel",
				"label" : "carousel"
			},
			{
				"url" : "/pages/gallery",
				"label" : "gallery"
			},
			{
				"url" : "/pages/heroImage",
				"label" : "heroImage"
			},
			{
				"url" : "/pages/imageText",
				"label" : "imageText"
			},
			{
				"url" : "/pages/infoBlocks",
				"label" : "infoBlocks"
			},
			{
				"url" : "/pages/infoBlocksImageCenter",
				"label" : "infoBlocksImageCenter"
			},
			{
				"url" : "/pages/jumbotron",
				"label" : "jumbotron"
			},
			{
				"url" : "/pages/map",
				"label" : "map"
			},
			{
				"url" : "/pages/mapContact",
				"label" : "mapContact"
			},
			{
				"url" : "/pages/textBlock",
				"label" : "textBlock"
			},
			{
				"url" : "/pages/textImage",
				"label" : "textImage"
			}
		]
	},
	{
		"url" : "/pages/contact-us",
		"label" : "contact us"
	}
]