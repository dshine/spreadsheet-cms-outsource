from flask import Flask
from flask import render_template
from flask_theme import setup_themes
#from flask_turbolinks import turbolinks

app = Flask(__name__)

#allow for loop controls in jinja template
app.jinja_env.add_extension('jinja2.ext.loopcontrols')
#add expires header to static files. Helps with page speed
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 60 * 60 * 24 * 7 
#turbolinks(app)

setup_themes(app, app_identifier="spreadsheetcms")

from app import views